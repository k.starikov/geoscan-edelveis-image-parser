#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
#include "CameraParser.hpp"
#include "DateConverter.hpp"

#define O_BINARY 0

struct CameraLogHandler {
	struct Header {
		uint16_t s;
		uint32_t t;
		uint16_t block;
	} __attribute__((packed));

	unsigned prevBlock{0};
	unsigned prevT{0};
	int fd {-1};
	unsigned fileCount{0};
	unsigned firstFileT{0};
	bool finish{false};

	void logMessage(const uint8_t *msg, uint32_t size, uint32_t block) {
		if (finish) {
			return ;
		}
		if (size < sizeof(Header)) {
			fprintf(stderr, "Wrong message size\n");
			return ;
		}
		const auto *hdr = (const Header *)msg;

		printf("%d %d\n", size, hdr->block);

		if (1 == hdr->block && 514 == size) { // start of jpeg file
			if (0 == firstFileT) {
				firstFileT = hdr->t;
			} else if (hdr->t == firstFileT) {
				fprintf(stderr, "First file again %d\n", hdr->t);
				finish = true;
				return ;
			}
			if (fd > 0) {
				close(fd);
				fd = -1;
			}
			char fileName[100];
			uint32_t sec = hdr->t % 60;
			uint32_t t = hdr->t / 60;
			uint32_t min = t % 60;
			t /= 60;
			uint32_t hour = t % 24;
			DateConverter::Date date(t / 24);
			sprintf(fileName, "Image_%02d-%02d-%02d_%02d-%02d-%02d.jpeg", date.year, date.month, date.day,
				hour, min, sec);
			fprintf(stderr, "New file detected, saving to %s\n", fileName);
			fd = open(fileName, O_CREAT | O_TRUNC | O_WRONLY | O_BINARY, 0666);
			if (fd < 0) {
				perror("");
			} else {
				write(fd, msg + sizeof(Header), size - sizeof(Header));
			}
			prevBlock = hdr->block;
		} else {
			if (hdr->block != prevBlock + 1) {
				fprintf(stderr, "Block sequence error\n");
				prevBlock = 0xffff;
			} else {
				prevBlock = hdr->block;
				if (fd >= 0) {
					write(fd, msg + sizeof(Header), size - sizeof(Header));
				}
			}
		}

	}

	~CameraLogHandler() {
		if (fd >= 0) {
			close(fd);
		}
	}
};

template<class Handler>
static void parse(int fd, Handler &handler) {
	uint8_t buffer[2048];
	CameraParser<2048> parser{};
	while (sizeof(buffer) == read(fd, buffer, sizeof(buffer))) {
		parser.parse(&handler, buffer, sizeof(buffer));
	}
	lseek(fd, 0, SEEK_SET);
	fprintf(stderr, "First sector\n");
	while (sizeof(buffer) == read(fd, buffer, sizeof(buffer))) {
		parser.parse(&handler, buffer, sizeof(buffer));
		if (handler.finish) {
			break;
		}
	}
}

int main(int argc, const char *argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Use: %s <frames.bin>\n", argv[0]);
		return 1;
	}
	int fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror(argv[1]);
		return 1;
	}
	CameraLogHandler handler;
    parse(fd, handler);
	close(fd);
}
