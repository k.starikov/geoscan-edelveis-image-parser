#ifndef CAMERA_PARSER_HPP
#define CAMERA_PARSER_HPP

#include <cstring>
#include <cstdint>

template<unsigned PageSize>
class CameraParser {
	unsigned msgSize;
	unsigned firstSize;
	unsigned block;
	uint8_t buffer[PageSize];

	template<class Handler>
	bool parseBlock(Handler *h, const uint8_t *data) {
		const uint8_t *end = data + PageSize;
		unsigned size = *(const uint16_t*)data;
		bool cont = size >> 15;
		size &= 0x7fff;
		if (cont) {
			if (msgSize && size + firstSize == msgSize + 2) {
				memcpy(buffer + firstSize, data + 2, size - 2);
				h->logMessage(buffer, msgSize, block);
				msgSize = 0;
				firstSize = 0;
			} else {
				 fprintf(stderr, "tail of record\n");
			}
			data += size;
		}
		while (data < end - 1) {
			unsigned size = *(const uint16_t*)data;
			if (size < 2) {
				break;
			} else if (data + size <= end) {
				memcpy(buffer, data, size);
				h->logMessage(buffer, size, block);
				data += size;
			} else if (size <= sizeof(buffer)) {
				msgSize = size;
				firstSize = end - data;
				memcpy(buffer, data, firstSize);
				return true;
			} else {
				return false;
			}
		}
		msgSize = firstSize = 0;
		return true;
	}

public:
	template<class Handler>
	void parse(Handler *h, const uint8_t *data, unsigned size) {
		const uint8_t *dataEnd = data + size;
		while (data < dataEnd) {
			if (!parseBlock(h, data)) {
				break;
			}
			data += PageSize;
			block++;
		}
	}
};

#endif
