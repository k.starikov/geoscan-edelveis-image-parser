#ifndef DATE_CONVERTER_HPP
#define DATE_CONVERTER_HPP

#include <cstdint>

struct DateConverter {
	static constexpr uint32_t daysFromCivil(uint32_t y, uint32_t m, uint32_t d) {
		y -= m <= 2;
		const uint32_t era = y / 400;
		const uint32_t yoe = y - era * 400;
		const uint32_t doy = (153 * (m > 2 ? m - 3 : m + 9) + 2) / 5 + d - 1;
		const uint32_t doe = yoe * 365 + yoe / 4 - yoe / 100 + doy;
		return era * 146097 + doe - 719468;
	}

	struct Date {
		uint32_t year{};
		uint32_t month{};
		uint32_t day{};

		constexpr explicit Date(uint32_t z) {
			z += 719468;
			const uint32_t era = z / 146097;
			const uint32_t doe = z - era * 146097; // [0, 146096]
			const uint32_t yoe = (doe - doe/1460 + doe/36524 - doe/146096) / 365; // [0, 399]
			year = yoe + era * 400;
			const uint32_t doy = doe - (365*yoe + yoe/4 - yoe/100); // [0, 365]
			const uint32_t mp = (5*doy + 2)/153; // [0, 11]
			day = doy - (153*mp+2)/5 + 1; // [1, 31]
			month = mp < 10 ? mp+3 : mp-9; // [1, 12]
			year += month <= 2;
		}
		constexpr Date(uint32_t y, uint32_t m, uint32_t d)
			: year(y)
			, month(m)
			, day(d)
		{
		}
		constexpr uint32_t days() const {
			return daysFromCivil(year, month, day);
		}
	};
};

#endif
