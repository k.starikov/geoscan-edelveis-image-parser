# Geoscan-Edelveis Image Parser

After you will recieve some frames from Geoscan-Edelveis there might be some images in them. You might check it  with this parser. 

## How to work with it

First of all merge all frames that you recieved from Geoscan-Edelveis

```
cat data* > frames.bin
```
("frames.bin" might have any name you want).

Then run parser in console:

```
./image-parser.o frames.bin
```

Image('s) will apprear in the directory in which image-parser.o is placed.
